package online.expotential.bazaar.client.canned

import org.apache.commons.csv.CSVFormat
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.springframework.core.io.DefaultResourceLoader
import java.io.InputStreamReader

object CannedClient {

    private const val file = "canned-orders.csv"
    private val url = "http://localhost:8080/limit-orders"
    private val client = HttpClients.createDefault()

    @JvmStatic
    fun main(args: Array<String>) {
        DefaultResourceLoader().getResource(file).inputStream.use { stream ->
            InputStreamReader(stream).use { reader ->
                CSVFormat.EXCEL
                    .withDelimiter(',')
                    .withHeader("instrumentId", "side", "price", "quantity")
                    .withSkipHeaderRecord(true)
                    .parse(reader)
                    .map { record ->
                        """
                        |{
                        |"instrumentId": "${record["instrumentId"]}",
                        |"side": "${record["side"]}",
                        |"price": "${record["price"]}",
                        |"quantity": "${record["quantity"]}"
                        |}
                    """.trimMargin()
                    }
                    .forEach { request ->
                        val post = HttpPost(url)
                        post.entity = StringEntity(request, ContentType.APPLICATION_JSON)
                        client.execute(post).close()
                    }
            }
        }

        client.close()
    }
}
