package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.clock.DefaultClockService
import online.expotential.bazaar.exchange.model.NewLimitOrderRequest
import online.expotential.bazaar.exchange.model.Side.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

internal class OrderBookTest {

    @Test
    fun testBlogPost3OrderBook() {

        val book = OrderBook("VOD.L", DefaultClockService())

        // Submit all of the orders
        with(book) {
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 438, 16.11))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 9, 19.45))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 12, 19.45))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 53, 17.32))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 123, 19.43))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 27, 17.32))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 153, 17.32))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 134, 18.47))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 99, 18.47))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 3, 11.1))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 32, 17.31))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 21, 16.11))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 2121, 17.31))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Sell, 12, 18.47))
            placeLimitOrder(NewLimitOrderRequest("VOD.L", Buy, 1, 16.11))
        }

        // Check the buy side of the order book
        assertEquals(
            OrderBookSide(
                Buy,
                linkedListOf(
                    OrderBookLevel(
                        17.32,
                        linkedListOf(
                            RestingOrder("3", "VOD.L", Buy, 53, 17.32, 53),
                            RestingOrder("5", "VOD.L", Buy, 27, 17.32, 27),
                            RestingOrder("6", "VOD.L", Buy, 153, 17.32, 153)
                        )
                    ),
                    OrderBookLevel(
                        17.31,
                        linkedListOf(
                            RestingOrder("10", "VOD.L", Buy, 32, 17.31, 32),
                            RestingOrder("12", "VOD.L", Buy, 2121, 17.31, 2121)
                        )
                    ),
                    OrderBookLevel(
                        16.11,
                        linkedListOf(
                            RestingOrder("0", "VOD.L", Buy, 438, 16.11, 438),
                            RestingOrder("11", "VOD.L", Buy, 21, 16.11, 21),
                            RestingOrder("14", "VOD.L", Buy, 1, 16.11, 1)
                        )
                    ),
                    OrderBookLevel(
                        11.1,
                        linkedListOf(
                            RestingOrder("9", "VOD.L", Buy, 3, 11.1, 3)
                        )
                    )
                )
            ),
            book.buys
        )

        // Check the sell side of the order book
        assertEquals(
            OrderBookSide(
                Sell,
                linkedListOf(
                    OrderBookLevel(
                        18.47,
                        linkedListOf(
                            RestingOrder("7", "VOD.L", Sell, 134, 18.47, 134),
                            RestingOrder("8", "VOD.L", Sell, 99, 18.47, 99),
                            RestingOrder("13", "VOD.L", Sell, 12, 18.47, 12)
                        )
                    ),
                    OrderBookLevel(
                        19.43,
                        linkedListOf(
                            RestingOrder("4", "VOD.L", Sell, 123, 19.43, 123)
                        )
                    ),
                    OrderBookLevel(
                        19.45,
                        linkedListOf(
                            RestingOrder("1", "VOD.L", Sell, 9, 19.45, 9),
                            RestingOrder("2", "VOD.L", Sell, 12, 19.45, 12)
                        )
                    )
                )
            ),
            book.sells
        )
    }

    private fun <T> linkedListOf(vararg items: T): LinkedList<T> = LinkedList<T>(items.asList())
}
