package online.expotential.bazaar.exchange.marketdata

import online.expotential.bazaar.exchange.Price
import online.expotential.bazaar.exchange.Quantity
import online.expotential.bazaar.exchange.TimestampUtcMillis

data class TradeMarketData(
    val price: Price,
    val quantity: Quantity,
    val timestampUtcMillis: TimestampUtcMillis
)
