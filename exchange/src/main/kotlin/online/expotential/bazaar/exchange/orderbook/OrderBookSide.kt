package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.model.Side
import java.util.*

/**
 * Either the buy or the sell side of an order book for a particular instrument
 *
 * @param side Whether this is the buy or the sell side of the order book
 * @param levelsByDecreasingAggression All levels on this side of the order book. On the buy side, highest prices are
 * first. For the sell side, lowest prices are first
 */
data class OrderBookSide(
    val side: Side,
    val levelsByDecreasingAggression: LinkedList<OrderBookLevel> = LinkedList()
)