package online.expotential.bazaar.exchange

object Util {
    /**
     * We'll use this all over the place for dealing with floating point precision
     */
    const val Epsilon = 1E-9
}