package online.expotential.bazaar.exchange

import online.expotential.bazaar.exchange.marketdata.Level2MarketData
import online.expotential.bazaar.exchange.marketdata.TradeMarketData
import online.expotential.bazaar.exchange.model.NewLimitOrderRequest
import online.expotential.bazaar.exchange.model.NewLimitOrderResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface Exchange {

    /**
     * @return A list of all instrument IDs known by the exchange
     */
    fun instruments(): Collection<InstrumentId>

    /**
     * Returns a reactive flux of level 2 market data for an instrument
     *
     * @param instrumentId The instrument ID for which to start receiving level 2 market data
     * @return A reactive flux of the level 2 market data
     */
    fun level2MarketDataForInstrumentId(instrumentId: InstrumentId): Flux<Level2MarketData>

    fun tradeMarketDataUpdatesForInstrumentId(instrumentId: InstrumentId): Flux<TradeMarketData>

    fun tradeMarketDataHistoryAndUpdatesForInstrumentId(instrumentId: InstrumentId): Flux<TradeMarketData>

    /**
     * Request placing a new limit order on the exchange
     *
     * @param request Details of the limit order to place
     * @return An asynchronous response wrapped in a reactive Mono
     */
    fun requestNewLimitOrder(request: NewLimitOrderRequest): Mono<NewLimitOrderResponse>
}
