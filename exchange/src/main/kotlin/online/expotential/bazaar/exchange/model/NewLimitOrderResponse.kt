package online.expotential.bazaar.exchange.model

import online.expotential.bazaar.exchange.ExchangeOrderId

data class NewLimitOrderResponse(
    val exchangeOrderId: ExchangeOrderId
)