package online.expotential.bazaar.exchange.clock

import online.expotential.bazaar.exchange.TimestampUtcMillis

interface ClockService {
    fun currentUtcMillis(): TimestampUtcMillis
}
