package online.expotential.bazaar.exchange.marketdata

import online.expotential.bazaar.exchange.Price
import online.expotential.bazaar.exchange.Quantity

data class Level2MarketDataRung(
    val price: Price,
    val quantity: Quantity
)
