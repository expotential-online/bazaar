package online.expotential.bazaar.exchange.model

enum class Side(val sign: Int) {
    Buy(1),
    Sell(-1)
}