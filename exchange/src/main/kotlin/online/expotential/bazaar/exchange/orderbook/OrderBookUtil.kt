package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.model.Side
import online.expotential.bazaar.exchange.model.Side.*
import java.lang.StringBuilder
import java.util.*

object OrderBookUtil {

    private const val headings = "SIDE |      PRICE |   QUANTITY | SHAPES ..."
    private const val headingsDivider = "-----+------------+------------+-----------"
    private const val basicsFormat = "%1$4s | %2$10s | %3$10s | "

    /**
     * Generates a semi-graphical representation of the state of the order book
     *
     * @param orderBook The order book to summarise
     * @return A summary of the levels and resting orders within the order book
     */
    fun summarise(orderBook: OrderBook): String {
        val sb = StringBuilder("Order Book for instrument [${orderBook.instrumentId}]").append("\n\n")
            .append(headings).append("\n")
            .append(headingsDivider)
        orderBook
            .buys.levelsByDecreasingAggression
            .reversed() // Lowest priced bids first so best bid and best offer appear next to each other (hopefully!)
            .forEach { l -> levelSummary(sb, Buy, l) }
        orderBook
            .sells.levelsByDecreasingAggression
            .forEach { l -> levelSummary(sb, Sell, l) }
        return sb.toString()
    }

    private fun levelSummary(sb: StringBuilder, side: Side, level: OrderBookLevel) {
        sb.append("\n")
        Formatter(sb).format(basicsFormat, side, level.price, level.calculateTotalQuantity())
        sb.append(level.restingOrdersByDecreasingPriority.joinToString(" ") { o -> o.remainingQuantity.toString() })
    }
}
