package online.expotential.bazaar.exchange.marketdata

data class Level2MarketData(
    val bidLadder: List<Level2MarketDataRung>,
    val askLadder: List<Level2MarketDataRung>
)
