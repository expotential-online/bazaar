package online.expotential.bazaar.exchange.clock

import online.expotential.bazaar.exchange.TimestampUtcMillis
import org.springframework.stereotype.Service
import java.time.Clock

@Service
class DefaultClockService : ClockService {
    override fun currentUtcMillis(): TimestampUtcMillis = Clock.systemUTC().millis()
}
