package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.ExchangeOrderId
import online.expotential.bazaar.exchange.InstrumentId
import online.expotential.bazaar.exchange.Quantity
import online.expotential.bazaar.exchange.Util.Epsilon
import online.expotential.bazaar.exchange.clock.ClockService
import online.expotential.bazaar.exchange.marketdata.Level2MarketData
import online.expotential.bazaar.exchange.marketdata.Level2MarketDataRung
import online.expotential.bazaar.exchange.marketdata.TradeMarketData
import online.expotential.bazaar.exchange.model.NewLimitOrderRequest
import online.expotential.bazaar.exchange.model.NewLimitOrderResponse
import online.expotential.bazaar.exchange.model.Side
import online.expotential.bazaar.exchange.model.Side.Buy
import online.expotential.bazaar.exchange.model.Side.Sell
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.ReplayProcessor
import java.util.concurrent.atomic.AtomicInteger

/**
 * The unique order book for a particular instrument
 *
 * @param instrumentId The ID of the instrument
 */
class OrderBook(
    val instrumentId: InstrumentId,
    @Autowired private val clockService: ClockService
) {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val nextExchangeOrderId = AtomicInteger(0)

    val buys = OrderBookSide(Buy)
    val sells = OrderBookSide(Sell)
    private val level2MarketData = ReplayProcessor.cacheLast<Level2MarketData>()
    private val tradeMarketDataUpdates = DirectProcessor.create<TradeMarketData>()
    private val tradeMarketDataHistoryAndUpdates = ReplayProcessor.create<TradeMarketData>()
    
    init {
        logger.info("Constructing new order book for instrument with ID [$instrumentId]")
    }

    fun level2MarketData(): Flux<Level2MarketData> = level2MarketData

    fun tradeMarketDataUpdates(): Flux<TradeMarketData> = tradeMarketDataUpdates

    fun tradeMarketDataHistoryAndUpdates(): Flux<TradeMarketData> = tradeMarketDataHistoryAndUpdates

    fun placeLimitOrder(request: NewLimitOrderRequest): NewLimitOrderResponse {

        // Conceptually all operations take place instantaneously
        val currentUtcMillis = clockService.currentUtcMillis()
        val exchangeOrderId = "${nextExchangeOrderId.getAndIncrement()}"
        var unplacedQuantity = request.quantity

        /*
         * STEP 1 - Try to match against resting orders on the far side of the order book
         */

        // We use iterators directly so that we can safely delete levels and orders during iteration
        val farLevels = farSideFor(request.side).levelsByDecreasingAggression.iterator()
        while (farLevels.hasNext()) {
            val farLevel = farLevels.next()
            val aggressionSurplus = request.side.sign * (request.price - farLevel.price)

            // If we the incoming order is more passive than this level of the far side then there are no more orders to
            // match and we can move on
            if (aggressionSurplus < -Epsilon)
                break

            val farOrders = farLevel.restingOrdersByDecreasingPriority.iterator()
            while (farOrders.hasNext()) {

                val farOrder = farOrders.next()

                if (unplacedQuantity >= farOrder.remainingQuantity) { // Far order fully filled against incoming order

                    // Publish trade market data
                    registerTradeMarketData(TradeMarketData(farLevel.price, farOrder.remainingQuantity, currentUtcMillis))

                    // There is now less of the incoming order remaining unfilled
                    unplacedQuantity -= farOrder.remainingQuantity

                    // Remove the far order from the level to which it belongs
                    farOrders.remove()

                    // If we just removed the last order resting at this level we can remove the level itself
                    if (farLevel.restingOrdersByDecreasingPriority.isEmpty()) {
                        farLevels.remove()
                        break
                    }

                    // The incoming order has been fully filled
                    if (unplacedQuantity == 0L)
                        return completeLimitOrderPlacement(exchangeOrderId)

                } else { // Far order partially filled against incoming order

                    // Publish trade market data
                    registerTradeMarketData(TradeMarketData(farLevel.price, unplacedQuantity, currentUtcMillis))

                    // The far order has less quantity remaining unfilled
                    farOrder.remainingQuantity -= unplacedQuantity

                    // The incoming order has been fully filled
                    return completeLimitOrderPlacement(exchangeOrderId)
                }
            }
        }

        /*
         * STEP 2 - Place any unplaced quantity as a resting order on the near side of the order book
         */

        // We use a list iterator because we can move back and forth and modify the collection
        val nearLevels = nearSideFor(request.side).levelsByDecreasingAggression.listIterator()
        while (nearLevels.hasNext() && unplacedQuantity > 0) {
            val level = nearLevels.next()
            val aggressionSurplus = request.side.sign * (request.price - level.price)
            when {
                aggressionSurplus > Epsilon -> {
                    // Incoming order price is more aggressive than the level we're considering
                    // For Buys...  order is HIGHER than level price
                    // For Sells... order is LOWER than level price
                    val newLevel = OrderBookLevel.newLevelFromRestingOrder(
                        restingOrder(request, exchangeOrderId, unplacedQuantity)
                    )
                    nearLevels.previous() // Addition on iterator adds AFTER the current element so we must move back
                    nearLevels.add(newLevel)
                    return completeLimitOrderPlacement(exchangeOrderId)
                }
                aggressionSurplus > -Epsilon -> {
                    // Incoming order price is within epsilon of the level we're considering so add to that level
                    level.restingOrdersByDecreasingPriority += restingOrder(request, exchangeOrderId, unplacedQuantity)
                    return completeLimitOrderPlacement(exchangeOrderId)
                }
            }
        }

        // Incoming order price is more passive than all levels so add to the bottom of the pile
        val newLevel = OrderBookLevel.newLevelFromRestingOrder(restingOrder(request, exchangeOrderId, unplacedQuantity))
        nearLevels.add(newLevel)
        return completeLimitOrderPlacement(exchangeOrderId)
    }

    private fun restingOrder(
        request: NewLimitOrderRequest,
        exchangeOrderId: ExchangeOrderId,
        unplacedQuantity: Quantity
    ) = with(request) {
        RestingOrder(exchangeOrderId, instrumentId, side, quantity, price, unplacedQuantity)
    }

    private fun completeLimitOrderPlacement(exchangeOrderId: ExchangeOrderId): NewLimitOrderResponse {
        logSummary()
        updateLevel2MarketData()
        return NewLimitOrderResponse(exchangeOrderId)
    }

    private fun registerTradeMarketData(update: TradeMarketData) {
        tradeMarketDataUpdates.onNext(update)
        tradeMarketDataHistoryAndUpdates.onNext(update)
    }

    private fun nearSideFor(side: Side) = when (side) {
        Buy -> buys
        Sell -> sells
    }

    private fun farSideFor(side: Side) = when (side) {
        Buy -> sells
        Sell -> buys
    }

    private fun updateLevel2MarketData() {
        level2MarketData.onNext(Level2MarketData(toLadder(buys), toLadder(sells)))
    }

    private fun toLadder(side: OrderBookSide): List<Level2MarketDataRung> =
        side.levelsByDecreasingAggression
            .map { Level2MarketDataRung(it.price, it.calculateTotalQuantity()) }
            .toList()

    private fun logSummary() {
        logger.info("\n\n${OrderBookUtil.summarise(this)}\n")
    }
}
