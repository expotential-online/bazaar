package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.ExchangeOrderId
import online.expotential.bazaar.exchange.InstrumentId
import online.expotential.bazaar.exchange.Price
import online.expotential.bazaar.exchange.Quantity
import online.expotential.bazaar.exchange.model.Side

/**
 * A limit order resting in an order book
 *
 * @param exchangeOrderId The unique ID by which the exchange knows the order
 * @param instrumentId The ID of the instrument to buy or sell
 * @param side The side of the order
 * @param quantity The number of units of the instrument to buy or sell
 * @param price The limit price of the order
 * @param remainingQuantity The quantity that remains to be executed
 */
data class RestingOrder(
    val exchangeOrderId: ExchangeOrderId,
    val instrumentId: InstrumentId,
    val side: Side,
    val quantity: Quantity,
    val price: Price,
    var remainingQuantity: Quantity
)
