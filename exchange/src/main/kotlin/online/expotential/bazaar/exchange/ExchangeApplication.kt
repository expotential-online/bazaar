package online.expotential.bazaar.exchange

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ExchangeApplication

fun main() {
    SpringApplication.run(ExchangeApplication::class.java)
}
