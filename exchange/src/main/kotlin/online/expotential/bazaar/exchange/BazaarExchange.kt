package online.expotential.bazaar.exchange

import online.expotential.bazaar.exchange.clock.ClockService
import online.expotential.bazaar.exchange.marketdata.Level2MarketData
import online.expotential.bazaar.exchange.marketdata.TradeMarketData
import online.expotential.bazaar.exchange.model.NewLimitOrderRequest
import online.expotential.bazaar.exchange.model.NewLimitOrderResponse
import online.expotential.bazaar.exchange.orderbook.OrderBook
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.util.concurrent.Callable
import java.util.concurrent.ConcurrentHashMap

/**
 * The core of the exchange implementation
 */
@Component
class BazaarExchange(
    @Autowired private val clockService: ClockService
) : Exchange {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val scheduler = Schedulers.newSingle("BazaarEx")
    private val orderBooksByInstrumentId = ConcurrentHashMap<InstrumentId, OrderBook>()

    override fun instruments(): Collection<InstrumentId> = orderBooksByInstrumentId.keys.sorted()

    override fun level2MarketDataForInstrumentId(instrumentId: InstrumentId): Flux<Level2MarketData> =
        orderBookForInstrumentId(instrumentId).level2MarketData()

    override fun tradeMarketDataUpdatesForInstrumentId(instrumentId: InstrumentId): Flux<TradeMarketData> =
        orderBookForInstrumentId(instrumentId).tradeMarketDataUpdates()

    override fun tradeMarketDataHistoryAndUpdatesForInstrumentId(instrumentId: InstrumentId): Flux<TradeMarketData> =
        orderBookForInstrumentId(instrumentId).tradeMarketDataHistoryAndUpdates()

    override fun requestNewLimitOrder(request: NewLimitOrderRequest): Mono<NewLimitOrderResponse> {
        logger.info("Exchange received $request")
        val callable = Callable { processNewLimitOrderRequest(request) }
        return Mono
            .fromCallable(callable)
            .publishOn(scheduler) // We'll do the actual work in the single scheduler thread
    }

    private fun processNewLimitOrderRequest(request: NewLimitOrderRequest): NewLimitOrderResponse {
        logger.info("Exchange is processing $request")
        return orderBookForInstrumentId(request.instrumentId).placeLimitOrder(request)
    }

    // The fact that we just create a new order book for every newly requested instrument leaves us open to injection
    // attacks so we should improve this in future
    private fun orderBookForInstrumentId(instrumentId: InstrumentId): OrderBook =
        orderBooksByInstrumentId.computeIfAbsent(instrumentId) { iid -> OrderBook(iid, clockService) }
}
