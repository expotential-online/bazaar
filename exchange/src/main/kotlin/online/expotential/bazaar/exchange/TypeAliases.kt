package online.expotential.bazaar.exchange

/**
 * Aliasing built-in types allows us to make our code more expressive while keeping it efficient
 */

typealias ExchangeOrderId = String
typealias InstrumentId = String
typealias Price = Double
typealias Quantity = Long
typealias TimestampUtcMillis = Long
