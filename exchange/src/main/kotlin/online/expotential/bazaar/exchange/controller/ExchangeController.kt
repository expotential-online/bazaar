package online.expotential.bazaar.exchange.controller

import online.expotential.bazaar.exchange.Exchange
import online.expotential.bazaar.exchange.InstrumentId
import online.expotential.bazaar.exchange.marketdata.Level2MarketData
import online.expotential.bazaar.exchange.marketdata.TradeMarketData
import online.expotential.bazaar.exchange.model.NewLimitOrderRequest
import online.expotential.bazaar.exchange.model.NewLimitOrderResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import java.time.Duration
import java.time.temporal.ChronoUnit.SECONDS

@RestController
class ExchangeController(
    @Autowired private val exchange: Exchange
) {
    private val logger = LoggerFactory.getLogger(javaClass)
    private val timeoutDuration = Duration.of(5, SECONDS)
    private val instrumentsPulseCycleDuration = Duration.of(1, SECONDS)

    @GetMapping("/instruments")
    fun instruments(): Flux<Collection<InstrumentId>> =
        Flux.interval(instrumentsPulseCycleDuration).map { exchange.instruments() }

    @GetMapping("/market-data/level2/{instrumentId}", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun level2(@PathVariable("instrumentId") instrumentId: InstrumentId): Flux<Level2MarketData> =
        exchange.level2MarketDataForInstrumentId(instrumentId)

    @GetMapping("/market-data/trades/updates/{instrumentId}", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun realtimeTrades(@PathVariable("instrumentId") instrumentId: InstrumentId): Flux<TradeMarketData> =
        exchange.tradeMarketDataUpdatesForInstrumentId(instrumentId)

    @GetMapping("/market-data/trades/history-and-updates/{instrumentId}", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun historicAndRealtimeTrades(@PathVariable("instrumentId") instrumentId: InstrumentId): Flux<TradeMarketData> =
        exchange.tradeMarketDataHistoryAndUpdatesForInstrumentId(instrumentId)

    @PostMapping("/limit-orders")
    fun requestNewLimitOrder(@RequestBody request: NewLimitOrderRequest): ResponseEntity<NewLimitOrderResponse> {
        logger.info("REST endpoint received $request")
        val response = exchange.requestNewLimitOrder(request)
            .timeout(timeoutDuration).block() // Give the exchange a little time to process our request
        logger.info("REST endpoint prepared response $response")
        return ResponseEntity.ok(response!!)
    }
}
