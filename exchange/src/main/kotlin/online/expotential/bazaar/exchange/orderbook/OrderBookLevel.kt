package online.expotential.bazaar.exchange.orderbook

import online.expotential.bazaar.exchange.Price
import java.util.*

/**
 * A priority-ordered collection of resting limit orders for the same instrument, side and price
 *
 * @param price The limit price of all resting orders at this level
 * @param restingOrdersByDecreasingPriority All resting orders at this level with the oldest submissions first
 */
data class OrderBookLevel(
    val price: Price,
    val restingOrdersByDecreasingPriority: Queue<RestingOrder> = LinkedList()
) {
    companion object {

        /**
         * Create a new level containing a single limit order
         *
         * @param restingOrder The single limit order to add to the new level
         */
        fun newLevelFromRestingOrder(restingOrder: RestingOrder): OrderBookLevel {
            val newLevel = OrderBookLevel(restingOrder.price)
            newLevel.restingOrdersByDecreasingPriority.add(restingOrder)
            return newLevel
        }
    }

    /**
     * @return The total quantity represented by all resting limit orders at this level
     */
    fun calculateTotalQuantity(): Long = restingOrdersByDecreasingPriority.map(RestingOrder::remainingQuantity).sum()
}
