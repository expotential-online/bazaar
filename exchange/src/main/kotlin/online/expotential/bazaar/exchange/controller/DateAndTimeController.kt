package online.expotential.bazaar.exchange.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@RestController
class DateAndTimeController {

    @GetMapping("/date-and-time/now")
    fun dateAndTimeNow(): ResponseEntity<String> =
        ResponseEntity.ok(ZonedDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
}