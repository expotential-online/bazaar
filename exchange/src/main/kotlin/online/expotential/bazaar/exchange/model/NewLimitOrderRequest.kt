package online.expotential.bazaar.exchange.model

import online.expotential.bazaar.exchange.InstrumentId
import online.expotential.bazaar.exchange.Price
import online.expotential.bazaar.exchange.Quantity

data class NewLimitOrderRequest(
    val instrumentId: InstrumentId,
    val side: Side,
    val quantity: Quantity,
    val price: Price
)