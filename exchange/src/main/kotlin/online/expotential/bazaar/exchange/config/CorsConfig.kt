package online.expotential.bazaar.exchange.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod.GET
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

@Configuration
class CorsConfig {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Bean
    fun corsFilter(): CorsFilter {
        logger.info("Applying CORS filter")
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()
        with (config) {
            allowCredentials = true
            addAllowedOrigin("*")
            addAllowedHeader("*")
            addAllowedMethod(GET)
        }
        source.registerCorsConfiguration("/**", config)
        return CorsFilter(source)
    }
}
