import { Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {

  // Our REST data source
  source: EventSource;

  cols = ['time', 'price', 'quantity'];

  trades = [];

  ngOnInit(): void {
    // Nothing to do
  }

  onInstrumentSelected(instrumentId) {

    if (this.source != null) {
      this.source.close();
      this.trades = [];
    }

    this.source = new EventSource(`http://${environment.exchangeRestHostPort}/market-data/trades/history-and-updates/${instrumentId}`);

    this.source.onmessage = m => {
      const json = JSON.parse(m.data);
      json.time = new Date(json.timestampUtcMillis).toLocaleTimeString();
      // See https://stackoverflow.com/questions/52992917/angular-data-binding-update-on-push-to-array-without-cloning-or-using-observable
      this.trades = this.trades.slice();
      // Use unshift instead of push to put newer trades at the top
      this.trades.unshift(json);
    };
  }
}
