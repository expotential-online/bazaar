import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-order-book',
  templateUrl: './order-book.component.html',
  styleUrls: ['./order-book.component.css']
})
export class OrderBookComponent implements OnInit {

  // Our REST data source
  source: EventSource;

  // Column headings for our bid and ask tables ordered so that their prices will appear centrally
  bidCols = ['bidQty', 'bidPx'];
  askCols = ['askPx', 'askQty'];

  // Arrays to contain the actual data we get from the REST data source
  bidLadder = [];
  askLadder = [];

  ngOnInit(): void {
    // Nothing to do
  }

  onInstrumentSelected(instrumentId) {

    // When a new instrument is selected we must cancel the previous subscription and blank the cache
    if (this.source != null) {
      this.source.close();
      this.bidLadder = [];
      this.askLadder = [];
    }

    // Subscribe to the level 2 market data REST endpoint at the exchange for this instrument
    this.source = new EventSource(`http://${environment.exchangeRestHostPort}/market-data/level2/${instrumentId}`);

    this.source.onmessage = m => {

      // Decode new messages from JSON and synchronise our local cache to update the tables
      const json = JSON.parse(m.data);
      this.bidLadder = json.bidLadder;
      this.askLadder = json.askLadder;
    };
  }
}
