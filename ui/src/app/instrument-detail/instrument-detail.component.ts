import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OrderBookComponent} from './order-book/order-book.component';
import {TradeHistoryComponent} from './trade-history/trade-history.component';

@Component({
  selector: 'app-instrument-detail',
  templateUrl: './instrument-detail.component.html',
  styleUrls: ['./instrument-detail.component.css']
})
export class InstrumentDetailComponent implements OnInit {

  instrumentIdSubscription: any;
  instrumentId: string;

  // Binds a reference to the child components for local access
  @ViewChild(OrderBookComponent, {static: true}) orderBook: OrderBookComponent;
  @ViewChild(TradeHistoryComponent, {static: true}) tradeHistory: TradeHistoryComponent;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // Angular Router passes the instrument ID to us in a map which we use to notify the order book component
    this.instrumentIdSubscription = this.route.paramMap.subscribe(params => {
      this.instrumentId = params.get('instrumentId');
      this.onInstrumentSelected(this.instrumentId);
    });
  }

  onInstrumentSelected(instrumentId) {
    // Tell order book component that a new instrument has been selected
    this.orderBook.onInstrumentSelected(instrumentId);
    this.tradeHistory.onInstrumentSelected(instrumentId);
  }
}
