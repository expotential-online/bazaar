import {Component, NgZone} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';

@Component({
  selector: 'app-instrument-selector',
  templateUrl: './instrument-selector.component.html',
  styleUrls: ['./instrument-selector.component.css']
})
export class InstrumentSelectorComponent {

  // Array of instruments we will obtain from the exchange's REST endpoint
  instruments = [];

  // Generated for us. This seems to drive the menu behaviour on mobile devices
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private zone: NgZone, private breakpointObserver: BreakpointObserver) {

    // Create an Observable representing a list of instruments that the exchange knows about
    new Observable<string[]>(observer => {

      // Subscribe to the available instruments REST endpoint at the exchange
      const source = new EventSource(`http://${environment.exchangeRestHostPort}/instruments`);

      source.onmessage = m => {

        // Emit the data decoded from its JSON representation
        this.zone.run(() => observer.next(JSON.parse(m.data)));
      };
    })
      // Synchronise our local array with the latest exchange view
      .subscribe(i => this.instruments = i);
  }
}
