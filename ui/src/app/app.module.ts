import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {InstrumentSelectorComponent} from './instrument-selector/instrument-selector.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {InstrumentDetailComponent} from './instrument-detail/instrument-detail.component';
import {OrderBookComponent} from './instrument-detail/order-book/order-book.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {RouterModule, Routes} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import { TradeHistoryComponent } from './instrument-detail/trade-history/trade-history.component';

// Define a single route to the instrument-detail component with a single instrumentId parameter
const appRoutes: Routes = [
  {
    path: 'instrument-detail/:instrumentId',
    component: InstrumentDetailComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    InstrumentSelectorComponent,
    InstrumentDetailComponent,
    OrderBookComponent,
    TradeHistoryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    // Reference the routing configuration variable above
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
